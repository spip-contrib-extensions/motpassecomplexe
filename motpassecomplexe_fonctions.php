<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Verification de la validite d'un mot de passe pour le mode d'auth concerne
 * c'est ici que se font eventuellement les verifications de longueur mini/maxi
 * ou de force.
 *
 * @param string $new_pass
 * @return string
 *  message d'erreur si login non valide, chaine vide sinon
 */
function motpassecomplexe_verifier_pass($new_pass, $longueur='', $min='', $maj='', $chi='', $spe='', $liste_spe='') {
	// charge les constantes
	if (!defined('_PASS_LONGUEUR_MINI')) define('_PASS_LONGUEUR_MINI', 6);			// longueur minimale - defaut: 6
	if (!defined('_MOTCOMPLEXE_MINUSCULE')) define('_MOTCOMPLEXE_MINUSCULE', 1);	// nb de minuscules  - defaut: 1
	if (!defined('_MOTCOMPLEXE_MAJUSCULE')) define('_MOTCOMPLEXE_MAJUSCULE', 1);	// nb de majuscules  - defaut: 1
	if (!defined('_MOTCOMPLEXE_CHIFFRE')) define('_MOTCOMPLEXE_CHIFFRE', 1);		// nb de chiffres  - defaut: 1
	if (!defined('_MOTCOMPLEXE_SPECIAL')) define('_MOTCOMPLEXE_SPECIAL', 1);		// nb de caractères spéciaux  - defaut: 1

	if (empty($longueur)) { $mini = _PASS_LONGUEUR_MINI; } else { $mini = $longueur; }
	if (empty($min)) { $min = _MOTCOMPLEXE_MINUSCULE; } else { $min = $min; }
	if (empty($maj)) { $maj = _MOTCOMPLEXE_MAJUSCULE; } else { $maj = $maj; }
	if (empty($chi)) { $chi = _MOTCOMPLEXE_CHIFFRE; } else { $chi = $chi; }
	if (empty($spe)) { $spe = _MOTCOMPLEXE_SPECIAL; } else { $spe = $spe; }

	$requis = array (
		'nb' => $mini,
		'min' => $min,
		'maj' => $maj,
		'chi' => $chi,
		'spe' => $spe,
	);

	$nb = strlen($new_pass);
	$nb_min = motpassecomplexe_count_pattern($new_pass, '![^a-z]+!');
	$nb_maj = motpassecomplexe_count_pattern($new_pass, '![^A-Z]+!');
	$nb_int = motpassecomplexe_count_pattern($new_pass, '![^0-9]+!');
	if (empty($liste_spe)) {
		$nb_spe = $nb - motpassecomplexe_count_pattern($new_pass, '![^A-Za-z0-9]+!');
	} else {
		$nb_spe = $nb - strlen(str_replace($liste_spe, "", $new_pass));
	}

	$verifications = array(
		array (
			'constante' => $mini,
			'operateur' => '>=',
			'variable' =>  $nb,
			'ch_lang' => 'nb'
		),
		array (
			'constante' => $min,
			'operateur' => '>=',
			'variable' =>  $nb_min,
			'ch_lang' => 'min'
		),
		array (
			'constante' => $maj,
			'operateur' => '>=',
			'variable' =>  $nb_maj,
			'ch_lang' => 'maj'
		),
		array (
			'constante' => $chi,
			'operateur' => '>=',
			'variable' =>  $nb_int,
			'ch_lang' => 'chi'
		),
		array (
			'constante' => $spe,
			'operateur' => '>=',
			'variable' =>  $nb_spe,
			'ch_lang' => 'spe'
		),
	);

	$item_langue = '';
	foreach ($verifications as $verification) {
		if ($requis[$verification['ch_lang']] > 0) {
			if (!version_compare($verification['variable'], $verification['constante'], $verification['operateur'])) {
				$item_langue .= '_' . $verification['ch_lang'];
			}
		}
	}
	if (!empty($item_langue)) {
		$item_langue = 'motpassecomplexe:info_passe_trop_court' . $item_langue;
		$item_langue = _T($item_langue, array('nb' => $mini, 'nb_min' => $min, 'nb_maj' => $maj, 'nb_int' => $chi, 'nb_spe' => $spe));
	}
	return $item_langue;
}

/**
 * Compte le nombre de caractères d'une chaine vérifiant une regex
 *
 * @param string $login
 *	chaine à verifier
 * @param string $pattern
 * 	motif regex à tester
 * @return int
 *	la longueur
 */
function motpassecomplexe_count_pattern($str, $pattern) {
	return strlen(preg_replace($pattern, '', $str));
}

/**
 * Filtre pour générer l'item de langue en fonction de la complexite du login
 *
 * @return string $item_langue
 *  Chaine de langue composer
 */
function filtre_langue_constante($mini, $min, $maj, $chi, $spe) {
	$item_langue = 'motpassecomplexe:info_passe_trop_court_nb';
	if ($min != '0') {
		$item_langue .= '_min';
	}
	if ($maj != '0') {
		$item_langue .= '_maj';
	}
	if ($chi != '0') {
		$item_langue .= '_chi';
	}
	if ($spe != '0') {
		$item_langue .= '_spe';
	}
	$item_langue = _T($item_langue, array('nb' => $mini, 'nb_min' => $min, 'nb_maj' => $maj, 'nb_int' => $chi, 'nb_spe' => $spe));
	return $item_langue;
}